#ifndef DIFFUSER_H
#define DIFFUSER_H

#include <armadillo>
#include <iomanip>
#include "LagPoly.hpp"

class Diffuser
{
	private:
		int n_e; // number of elements
		int n_n; // number of nodes
		int n_l; // number of loose nodes (without continuity)
		LagPoly poly;
		arma::sp_fmat mMat; // mass
		arma::sp_fmat kMat; // stiffness

	private:
		arma::sp_umat computeC();
		void computeM(const arma::umat &, const arma::sp_umat &, const arma::fvec &);
		void computeK(const arma::umat &, const arma::sp_umat &, const arma::fvec &, const arma::fvec &);
		arma::umat sparseIndexes();

	public:
		Diffuser();
		Diffuser(int, int, const arma::fvec &, const arma::fvec &); //pol order, nb of elements, elements thickness array, diffusivity array
		void tempStepping(float, arma::sp_fmat &, arma::sp_fmat &, char='f');
		arma::fvec rungeKutta2Stepping(float, const arma::fvec &);
		arma::fvec rungeKutta4Stepping(float, const arma::fvec &);
		LagPoly getPoly();
		arma::sp_fmat getM();
		void setM(arma::fmat); //TODO
		arma::sp_fmat getK();
		void setK(arma::fmat, arma::fmat); //TODO
		arma::fmat getMP();
};

#endif /* DIFFUSER_H */

