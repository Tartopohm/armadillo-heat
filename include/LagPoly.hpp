#ifndef LAGPOLY_H
#define LAGPOLY_H

#include <armadillo>

class LagPoly
{
	private:
		int degree; // degree of polynomials
		arma::fmat mMat; // reference  mass matrix
		arma::fmat kMat; // reference stifness matrix

	private:
		void setM();
		void setK();

	public:
		LagPoly();
		LagPoly(int);
		arma::fmat getM();
		arma::fmat getK();
		int getDegree();
};

#endif /* LAGPOLY_H */
