#ifndef SNOWSLAB_H
#define SNOWSLAB_H

#include <iomanip>
#include <armadillo>
#include "Diffuser.hpp"

#define ICE_D 1.2E-6f //ice diffusivity, m2/s
#define AIR_D 2E-5f //air diffusivity, m2/s
#define SID 86400 // seconds in day
#define KS 273.15f // Kelvin shift

class SnowSlab
{
	private:
		int n_e; // n elements
		int n_n; // n nodes
		float thickness; // meters
		arma::fvec zArray; // coordinates of nodes (m), rows index
		arma::fvec tArray; // time, cols index
		arma::uvec iceArray; // 1: ice ; 0: air
		arma::fmat temperature;
		Diffuser diff; // m2/s

	private:
		arma::fvec initZArray(float, int, int);
		arma::fvec initThicknessArray(const int, const int);
		arma::uvec initIceArray(int, float, int);
		arma::fvec initDiffusivityArray();
		arma::uvec hhIndices(int);

	public:
		SnowSlab(int, arma::fvec, float=1., float=.4, int=2, int='n'); // nb of elements, initial condition, thickness, ice fraction, degree of polynomial
		int getNE();
		float getThickness();
		arma::fvec getZArray();
		arma::uvec getIceArray();
		arma::fmat getTemperature();
		Diffuser getDiffuser();
		void populate(float, float, float, float, char, float=0); // dt, duration, minTemp, maxTemp, method tu use, shift
		void writeTemp(int=1800);
		void writeTemp(std::string, int=1800);
		void reset();
};

#endif /* SNOWSLAB_H */
