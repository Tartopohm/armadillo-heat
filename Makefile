CXX := g++
CXXFLAGS := -std=c++11 -Wall -O2 -g
LDFLAGS := -larmadillo
BINDIR := bin
EXEC = $(BINDIR)/exec

SRCDIR := src
SRCEXT := cpp
SRC := $(shell ls $(SRCDIR)/*.$(SRCEXT))
OBJDIR := build
OBJEXT := o
OBJ := $(patsubst $(SRCDIR)/%,$(OBJDIR)/%,$(SRC:.$(SRCEXT)=.$(OBJEXT)))
#OBJ = $(SRC:.c=.o)
INC := -I include


all: $(EXEC)

$(EXEC): $(OBJ)
	@echo "Linking..."
	@echo "  $(CXX) $(LDFLAGS) $^ -o $(EXEC)"
	@$(CXX) $(LDFLAGS) $^ -o $(EXEC)

$(OBJDIR)/%.$(OBJEXT): $(SRCDIR)/%.$(SRCEXT)
	@echo "Compiling $<..."
	@echo "  $(CXX) $(CXXFLAGS) $(INC) -c -o $@ $<"
	@$(CXX) $(CXXFLAGS) $(INC) -c -o $@ $<

clean:
	rm -f $(OBJDIR)/*.$(OBJEXT)

mrproper: clean
	rm -f $(BINDIR)/*
