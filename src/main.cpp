#include <iostream>
#include <armadillo>
#include "LagPoly.hpp"
#include "Diffuser.hpp"
#include "SnowSlab.hpp"

int main()
{
	const char meth[] = {'f', 'b', 'n', 'm', 'r'}; // schémas de migration en temps
	int n_e = 100; // nombre d'éléments
	int deg = 1; // degré polynomial
	float th = 1; // épaisseur de neige (m)
	float f = 0.36; // proportion de glace
	arma::fvec temp0 = -6*arma::ones<arma::fvec>(n_e*deg+1); // condition initiale (°C)
	float dt = pow((th/n_e), 2)/AIR_D/6; // pas de temps d'itération

	SnowSlab ss = SnowSlab(n_e, temp0, th, f, deg, 3749);
	ss.populate(dt, 3*SID, -11, -1, meth[1]); // itérations pendant trois jours, forçage sinusoïdale entre -11 et -1 °C
	ss.writeTemp(); // écriture dans un fichier

	return EXIT_SUCCESS;
}
