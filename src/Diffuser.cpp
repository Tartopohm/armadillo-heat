#include <iostream>
#include "Diffuser.hpp"
#include "LagPoly.hpp"

Diffuser::Diffuser()
{
}

Diffuser::Diffuser(int lp_degree, int n_elements, const arma::fvec &thicknessArr, const arma::fvec &diffusivityArr) : n_e(n_elements), n_n(lp_degree*n_elements + 1), n_l((lp_degree + 1)*n_elements), poly(lp_degree)
{
	const arma::umat locations = this->sparseIndexes();
	const arma::sp_umat connec = this->computeC();

	std::cout << "  Computing mass matrix..." << std::endl;
	this->computeM(locations, connec, thicknessArr);
	std::cout << "   done." << std::endl;

	std::cout << "  Computing stiffness  matrix..." << std::endl;
	this->computeK(locations, connec, thicknessArr, diffusivityArr);
	std::cout << "   done." << std::endl;
}

arma::sp_umat Diffuser::computeC()
{
	const int deg = this->poly.getDegree();
	/* Rows and columns where C != 0.
	 * One value (1) per row, sometimes two per column, depending on d.
	 */
	const arma::urowvec rows = arma::linspace<arma::urowvec>(0, n_l-1, n_l);
	const arma::urowvec cols = rows -  arma::vectorise(arma::repmat(arma::linspace<arma::urowvec>(0, this->n_e-1, this->n_e), deg+1, 1)).t(); 

	return arma::sp_umat(arma::join_cols(rows, cols), arma::uvec(this->n_l, arma::fill::ones));
}
/*
 * Compute the row and column indexes where mass and stifness matrices contain actual values.
 */
arma::umat Diffuser::sparseIndexes()
{

	int n = this->getPoly().getDegree() + 1;
	arma::umat locations(2, n*this->n_l, arma::fill::zeros);
	for(int i = 0 ; i < this->n_l ; i++)
	{
		for(int j = 0 ; j < n ; j++)
		{
			locations(0, i*n + j) = i;
			locations(1, i*n + j) = i - i%n + j;
		}
	}

	return locations;
}

void Diffuser::computeM(const arma::umat &locations, const arma::sp_umat &connectivity, const arma::fvec &thicknessArr)
{
	const int np = this->poly.getDegree() + 1;
	arma::fmat block = arma::repmat(this->poly.getM(), 1, this->n_e);
	
	/*
	 * Reference mass matrix scaled due to the integration by substitution.
	 */
	for(int i = 0 ; i < n_e ; i++)
		block.submat(0, np*i, np-1, np*i + np-1) *= thicknessArr[i]/2;
		
	this->mMat = connectivity.t()*arma::sp_fmat(locations, arma::vectorise(block))*connectivity;
}

void Diffuser::computeK(const arma::umat &locations, const arma::sp_umat &connectivity, const arma::fvec &thicknessArr, const arma::fvec &diffusivityArr)
{
	const int np = this->poly.getDegree() + 1;
	arma::fmat block = arma::repmat(this->poly.getK(), 1, this->n_e);

	/*
	 * Reference stifness  matrix scaled due to the integration by substitution.
	 */
	for(int i = 0 ; i < n_e ; i++)
		block.submat(0, np*i, np-1, np*i + np-1) *= diffusivityArr[i]/(thicknessArr[i]/2);
	
	this->kMat = connectivity.t()*arma::sp_fmat(locations, arma::vectorise(block))*connectivity;
}

/*
 * Encapsulates EF, EB and CN schemes.
 * */
void Diffuser::tempStepping(float dt, arma::sp_fmat &matA, arma::sp_fmat &matB, char method)
{
	switch(method)
	{
		case 'f':
			std::cout << "Euler forward scheme..." << std::endl;
			matA = this->mMat;
			matB = this->mMat - dt*this->kMat;
			break;
		case 'b':
			std::cout << "Euler backward scheme..." << std::endl;
			matA = this->mMat + dt*this->kMat;
			matB = this->mMat;
			break;
		case 'n':
			std::cout << "Crank-Nicolson scheme..." << std::endl;
			matA = 2*this->mMat + dt*this->kMat;
			matB = 2*this->mMat - dt*this->kMat;
			break;
	}
}

/*
 * Encapsulates RK2 scheme.
 */
arma::fvec Diffuser::rungeKutta2Stepping(float dt, const arma::fvec &temp)
{
	return dt*arma::spsolve(this->mMat, -this->kMat*(temp + dt*arma::spsolve(this->mMat, -this->kMat*temp)/2));
}

/*
 * Encapsulates RK4 scheme.
 */
arma::fvec Diffuser::rungeKutta4Stepping(float dt, const arma::fvec &temp)
{
	arma::fvec p1 = arma::spsolve(this->mMat, -this->kMat*temp);
	arma::fvec p2 = arma::spsolve(this->mMat, -this->kMat*(temp + dt*p1/2));
	arma::fvec p3 = arma::spsolve(this->mMat, -this->kMat*(temp + dt*p2/2));
	arma::fvec p4 = arma::spsolve(this->mMat, -this->kMat*(temp + dt*p3));

	return dt*(p1 + 2*p2 + 2*p3 + p4)/6;
}

LagPoly Diffuser::getPoly()
{
	return this->poly;
}

arma::sp_fmat Diffuser::getM()
{
	return this->mMat;
}

arma::sp_fmat Diffuser::getK()
{
	return this->kMat;
}

