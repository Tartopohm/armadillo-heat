#include "LagPoly.hpp"

LagPoly::LagPoly()
{
}

LagPoly::LagPoly(int p_degree) : degree(p_degree)
{
	this->setM();
	this->setK();
}

void LagPoly::setM()
{
	switch(this->degree)
	{
		case 1:
			this->mMat = arma::fmat({
					{2, 1},
					{1, 2}})/3;
			break;
		case 2:
			this->mMat = arma::fmat({
					{4, 2, -1},
					{2, 16, 2},
					{-1, 2, 4}})/15;
			break;
		case 3:
			this->mMat = arma::fmat({
					{128, 99, -36, 19},
					{99, 648, -81, -36},
					{-36, -81, 648, 99},
					{19, -36, 99, 128}})/840;
			break;
		case 4:
			this->mMat = arma::fmat({
					{292, 296, -174, 56, -29},
					{296, 1792, -384, 256, 56},
					{-174, -384, 1872, -384, -174},
					{56, 256, -384, 1792, 296},
					{-29, 56, -174, 296, 292}})/2835;
			break;
	}
}

void LagPoly::setK()
{
	switch(this->degree)
	{
		case 1:
			this->kMat = arma::fmat({
					{1, -1},
					{-1, 1}})/2;
			break;
		case 2:
			this->kMat = arma::fmat({
					{3.5, -4, .5},
					{-4, 8, -4},
					{.5, -4, 3.5}})/3;
			break;
		case 3:
			this->kMat = arma::fmat({
					{148, -189, 54, -13},
					{-189, 432, -297, 54},
					{54, -297, 432, -189},
					{-13, 54, -189, 148}})/80;
			break;
		case 4:
			this->kMat = arma::fmat({
					{4925, -6848, 3048, -1472, 347},
					{-6848, 16640, -14208, 5888, -1472},
					{3048, -14208, 22320, -14208, 3048},
					{-1472, 5888, -14208, 16640, -6848},
					{347, -1472, 3048, -6848, 4925}})/1890;
	}
}

arma::fmat LagPoly::getM()
{
	return this->mMat;
}

arma::fmat LagPoly::getK()
{
	return this->kMat;
}

int LagPoly::getDegree()
{
	return this->degree;
}
