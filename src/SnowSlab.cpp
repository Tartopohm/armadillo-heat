#include <fstream>
#include "SnowSlab.hpp"

SnowSlab::SnowSlab(int n_elements, arma::fvec initTemp, float slabThickness, float iceFraction, int p_degree, int seed) : n_e(n_elements), n_n(n_elements*p_degree + 1), thickness(slabThickness), zArray(this->initZArray(thickness, n_elements, p_degree)), tArray(arma::zeros<arma::fvec>(1)), iceArray(this->initIceArray(n_elements, iceFraction, seed)), temperature(initTemp+273.15), diff(p_degree, n_elements, this->initThicknessArray(n_elements, p_degree), this->initDiffusivityArray())
{
			
}

arma::fvec SnowSlab::initZArray(float th, int n_e, int deg)
{
	return arma::linspace<arma::fvec>(-th, 0, n_e*deg + 1);
}

/*
 * Computes the h_e s.
 */
arma::fvec SnowSlab::initThicknessArray(int n_e, int deg)
{
	arma::uvec i0 = arma::uvec(n_e);
	arma::uvec i2 = arma::uvec(n_e);

	for(int i = 0 ; i < n_e ; i++)
	{
		i0[i] = deg*i;
		i2[i] = deg*(i+1);
	}

	return this->zArray(i2) - this->zArray(i0);
}

/*
 * Random distribution of ice, according to f (iceFra) and a random seed.
 */
arma::uvec SnowSlab::initIceArray(int n_e, float iceFra, int seed)
{
	if(iceFra == 1)
		return arma::ones<arma::uvec>(n_e);

	if(iceFra == 0)
		return arma::zeros<arma::uvec>(n_e);
	
	if(seed != 'n')
		arma::arma_rng::set_seed(seed);
	else
		arma::arma_rng::set_seed_random();

	arma::fvec randAr = arma::fvec(n_e, arma::fill::randu);
	arma::uvec iceArray = (randAr <= (iceFra - 2./n_e));
	iceArray(0) = 1;
	iceArray(n_e-1) = 1;
	return iceArray;
}

/*
 * Element-wise diffusivities according to the ice distribution.
 */
arma::fvec SnowSlab::initDiffusivityArray()
{
	return ICE_D*arma::conv_to<arma::fvec>::from(this->iceArray) + AIR_D*arma::conv_to<arma::fvec>::from(this->iceArray != 1);
}

/*
 * User's interface to time series computation.
 */
void SnowSlab::populate(float dt, float duration, float minTemp, float maxTemp, char meth, float shift)
{
	const int N_STEPS = (int)std::ceil(duration/dt);
	const int width = std::to_string(N_STEPS).length();
	const int pCols = this->temperature.n_cols; // previous number of columns
	int n = 1;
	minTemp += KS;
	maxTemp += KS;

	std::cout << "  Allocation... ";
	this->temperature.resize(this->temperature.n_rows, pCols + N_STEPS);
	this->tArray.resize(pCols + N_STEPS);
	std::cout << "done." << std::endl;

	/*
	 * Method switch.
	 */
	std::cout << "  Time stepping, using ";
	if(meth=='f' || meth=='b' || meth=='n')
	{
		arma::sp_fmat aMat, bMat;
		this->getDiffuser().tempStepping(dt, aMat, bMat, meth);
		for(int i = pCols ; i < pCols + N_STEPS ; i++)
		{
			std::cout << "\r   " << std::setw(width) << n << "/" << N_STEPS << " (" << std::setw(3) << 100*n/N_STEPS << "%)..." << std::flush;
			float fTemp = ((maxTemp - minTemp)*sin(2*arma::datum::pi*(n*dt - shift)/SID) + maxTemp + minTemp)/2;
			n++;
			this->tArray(i) = tArray(i-1) + dt;
			this->temperature.col(i) = arma::spsolve(aMat, bMat*this->temperature.col(i-1), "superlu");
			this->temperature.col(i)[this->n_n - 1] = fTemp;
		}
	}
	else if(meth=='m')
	{
		std::cout << "Runge-Kutta 2 scheme..." << std::endl;
		for(int i = pCols ; i < pCols + N_STEPS ; i++)
		{
			std::cout << "\r   " << std::setw(width) << n << "/" << N_STEPS << " (" << std::setw(3) << 100*n/N_STEPS << "%)..." << std::flush;
			float fTemp = ((maxTemp - minTemp)*sin(2*arma::datum::pi*(n*dt - shift)/SID) + maxTemp + minTemp)/2;
			n++;
			this->tArray(i) = tArray(i-1) + dt;
			this->temperature.col(i) = this->temperature.col(i-1) + this->diff.rungeKutta2Stepping(dt, this->temperature.col(i-1));
			this->temperature.col(i)[this->n_n - 1] = fTemp;
		}	
	}
	else if(meth=='r')
	{
		std::cout << "Runge-Kutta 4 scheme..." << std::endl;
		for(int i = pCols ; i < pCols + N_STEPS ; i++)
		{
			std::cout << "\r   " << std::setw(width) << n << "/" << N_STEPS << " (" << std::setw(3) << 100*n/N_STEPS << "%)..." << std::flush;
			float fTemp = ((maxTemp - minTemp)*sin(2*arma::datum::pi*(n*dt - shift)/SID) + maxTemp + minTemp)/2;
			n++;
			this->tArray(i) = tArray(i-1) + dt;
			this->temperature.col(i) = this->temperature.col(i-1) + this->diff.rungeKutta4Stepping(dt, this->temperature.col(i-1));
			this->temperature.col(i)[this->n_n - 1] = fTemp;
		}
	}

	std::cout << " done." << std::endl;
}

/*
 * Wirtes to file, with a given filename or after creating a date-based one.
 */
void SnowSlab::writeTemp(int timeResolution)
{
	std::time_t at = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	char fileName[23];
	std::strftime(fileName, sizeof(fileName), "ST_%Y%m%d_%H%M%S.csv", std::localtime(&at));
	this->writeTemp(std::string(fileName), timeResolution);
}

void SnowSlab::writeTemp(std::string fileName, int timeResolution)
{
	const arma::uvec INDICES = this->hhIndices(timeResolution);
	const int N_COLS = INDICES.size();
	const int N_ROWS = this->zArray.size();
	const arma::fvec SUB_TIME = this->tArray.rows(INDICES);
	const arma::fmat SUB_TEMP = this->temperature.cols(INDICES) - 273.15;

	std::ofstream file;
	file.open(fileName, std::ios::out);

	std::stringstream header;
	header << "z,isIce";
	for(int i = 0 ; i < N_COLS ; i++)
		header << "," << SUB_TIME[i];
	header << "\n";
	file << header.str();
	
	const int d = this->diff.getPoly().getDegree();
	const int width = std::to_string(N_ROWS).length();

	std::cout << "  Writing file " << fileName << ":" << std::endl;
	for(int i = 0 ; i < N_ROWS ; i++)
	{
		std::cout << "\r   " << std::setw(width) << i+1 << "/" << N_ROWS << " (" << std::setw(3) << 100*(i+1)/N_ROWS << "%)..." << std::flush;
		std::stringstream line;
		line << zArray[i] << ",";
		if(i < N_ROWS-d && i%d == 0)
			line << iceArray[i/d];
		for(int j = 0 ; j < N_COLS ; j++)
			line << "," << SUB_TEMP(i, j);
		line << "\n";
		file << line.str();
	}
	std::cout << " done." << std::endl;
	
	file.close();
}

/*
 * Temperature matrice cropping to keep only the timesteps corresponding to a given time resolution.
 */
arma::uvec SnowSlab::hhIndices(int timeResolution)
{
	const int MAX_SIZE = this->tArray.size();
	const float LAST_TS = this->tArray[MAX_SIZE - 1];
	const int N_STEPS = (int)(LAST_TS/timeResolution) + 1;
	arma::uvec ia = arma::zeros<arma::uvec>(N_STEPS); // indexes array

	for(int i = 0 ; i < N_STEPS ; i++)
	{
		float t = i*timeResolution;
		ia[i] = arma::abs(this->tArray - t).index_min();
	}

	return ia;
}

/*
 * Puts SnowSlab back to its original state.
 */
void SnowSlab::reset()
{
	this->temperature = this->temperature.col(0);
	this->tArray = arma::zeros<arma::fvec>(1);
}


int SnowSlab::getNE()
{
	return this->n_e;
}
float SnowSlab::getThickness()
{
	return this->thickness;
}

Diffuser SnowSlab::getDiffuser()
{
	return this->diff;
}

arma::fvec SnowSlab::getZArray()
{
	return this->zArray;
}

arma::uvec SnowSlab::getIceArray()
{
	return this->iceArray;
}

arma::fmat SnowSlab::getTemperature()
{
	return this->temperature;
}
